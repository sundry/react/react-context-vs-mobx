import React from 'react';
import Counter from '../Counter/Counter';
import CounterReducer from '../CounterReducer/Counter';
import CounterMobx from '../CounterMobx/Counter';
import NameBox from '../NameBox/NameBoxWithStore';

function App() {
  return (
    <div style={
      {
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center'
      }
    }>
      {/* <div style={{ margin: 10 }}><Counter /></div>
      <div style={{ margin: 10 }}><CounterReducer /></div>
      <div style={{ margin: 10 }}><CounterMobx /></div> */}
      <div style={{ margin: 10 }}><NameBox /></div>
    </div>
  );
}

export default App;
