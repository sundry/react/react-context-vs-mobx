import React from 'react';
import { withProvider } from './store/Context';
import useCounter from './store/useCounter';

const Header = ({ count }) => {
  return <h1>{count}</h1>;
}

const Actions = ({ increment, decrement }) => {
  return (
    <>
      <button onClick={increment}>+</button>
      <button onClick={decrement}>-</button>
    </>
  )
}

const OtherInfo = () => {
  return (
    <div>Other information</div>
  );
};

const Counter = () => {
  const store = useCounter();
  return (
    <div>
      <Header count={store.count} />
      <Actions increment={store.increment} decrement={store.decrement} /> 
      <OtherInfo />
      <div>CounterReducer</div>
    </div>
  );
}

export default withProvider(Counter);
