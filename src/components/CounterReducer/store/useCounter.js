import { useStore } from './Context';

const useCounter = () => {
  const [state, dispatch] = useStore();

  function increment() {
    dispatch('increment');
  }

  function decrement() {
    dispatch('decrement');
  }
  
  return {
    count: state.count,
    info: state.info,
    increment,
    decrement,
  }
};

export default useCounter;