import React, { useReducer, useContext } from 'react';

const Context = React.createContext([{}, () => {}]);

const initialState = {
  count: 0,
};
const reducer = (state, action) => {
  switch (action) {
    case 'increment': return { ...state, count: state.count + 1 };;
    case 'decrement': return { ...state, count: state.count - 1 };;
    case 'reset': return { ...state, count: 0 };
    default: throw new Error('Unexpected action');
  }
};

const Provider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Context.Provider value={[state, dispatch]}>
      {props.children}
    </Context.Provider>
  );
}

export const useStore = () => useContext(Context);

export function withProvider(Component) {
  return function WrapperComponent(props) {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
}

export { Context, Provider };