import React from 'react';
import { withProvider } from './store/Context';
import useCounter from './store/useCounter';

const Header = () => {
  const { count } = useCounter();
  return <h1>{count}</h1>;
}

const Actions = () => {
  const { increment, decrement } = useCounter();
  return (
    <>
      <button onClick={increment}>+</button>
      <button onClick={decrement}>-</button>
    </>
  )
}

const OtherInfo = () => {
  const { info } = useCounter();
  return (
    <div>Other information - {info}</div>
  );
};

const Counter = () => {
  return (
    <div>
      <Header />
      <Actions /> 
      <OtherInfo />
      <div>Counter</div>
    </div>
  );
}

export default withProvider(Counter);
