import { useStore } from "./Context";

const useCounter = () => {
  const [state, setState] = useStore();

  function increment() {
    setState(state => ({ ...state, count: state.count + 1 }));
  }

  function decrement() {
    setState(state => ({ ...state, count: state.count - 1 }));
  }
  
  return {
    count: state.count,
    info: state.info,
    increment,
    decrement,
  }
};

export default useCounter;