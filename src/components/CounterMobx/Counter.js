import React from 'react';
import { observer } from 'mobx-react';
import Store from './store/Store';

const Header = observer(() => {
  return <h1>{Store.count}</h1>;
});

const Actions = observer(() => {
  return (
    <>
      <button onClick={() => Store.increment()}>+</button>
      <button onClick={() => Store.decrement()}>-</button>
    </>
  )
});

const OtherInfo = () => {
  return (
    <div>Other information</div>
  );
};

const Counter = () => {
  return (
    <div>
      <Header />
      <Actions /> 
      <OtherInfo />
      <div>CounterMobx</div>
    </div>
  );
}

export default Counter;
