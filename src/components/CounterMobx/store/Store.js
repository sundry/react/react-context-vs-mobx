import { observable, decorate } from 'mobx';

class Store {
  count = 0;
  increment() {
    this.count += 1;
  }
  decrement() {
    this.count -= 1;
  };
}

export default decorate(new Store(), {
  count: observable,
});
