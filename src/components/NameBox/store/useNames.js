import { useStore } from "./Context";

const useNames = () => {
  const [state, setState] = useStore();

  function addName(name) {
    const names = [...state.names, name];
    setState(state => ({ ...state, names }));
  }
  
  return {
    names: state.names,
    addName,
  }
};

export default useNames;