import React, { useState } from 'react';

const Names = ({ names }) => names.map(name => <div>{name}</div>);

const AddName = ({ addName }) => {
  const [name, setName] = useState('');
  return (
    <div>
      <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      <button onClick={() => addName(name)}>Add</button>
    </div>
  );
};

function NameBox() {
  const [names, setNames] = useState([]);
  const addName = (name) => setNames([...names, name]);
  let b;
  for (let i = 0; i < 100000000; i++) {
    b = i;
  }
  console.log(b);
  return (
    <div>
      <AddName addName={addName} />
      <Names names={names} />
    </div>
  );
}

export default NameBox;
