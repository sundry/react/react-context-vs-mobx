import React, { useState } from 'react';
import { withProvider } from './store/Context';
import useNames from './store/useNames';

const Names = () => {
  const { names } = useNames();
  return names.map(name => <div>{name}</div>);
};

const AddName = () => {
  const { addName } = useNames();
  const [name, setName] = useState('');
  return (
    <div>
      <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      <button onClick={() => addName(name)}>Add</button>
    </div>
  );
};

function NameBox() {
  let b;
  for (let i = 0; i < 1000000000; i++) {
    b = i;
  }
  console.log(b);
  return (
    <div>
      <AddName />
      <Names />
    </div>
  );
}

export default withProvider(NameBox);
